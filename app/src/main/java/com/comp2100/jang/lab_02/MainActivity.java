package com.comp2100.jang.lab_02;

import static com.comp2100.jang.lab_02.Expression.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public double numberOne = 0;
    public double numberTwo = 0;

    public String temp = "";
    public TextView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        display = (TextView) findViewById(R.id.display);
        display.setText("0");
    }

    public void zero(View v) {
        if (display.getText().equals("0"))
            temp = "0";
        else
            temp = display.getText() + "0";
        display.setText(temp);
    }

    public void one(View v) {
        if (display.getText().equals("0"))
            temp = "1";
        else
            temp = display.getText() + "1";
        display.setText(temp);
    }

    public void two(View v) {
        if (display.getText().equals("0"))
            temp = "2";
        else
            temp = display.getText() + "2";
        display.setText(temp);
    }

    public void three(View v) {
        if (display.getText().equals("0"))
            temp = "3";
        else
            temp = display.getText() + "3";
        display.setText(temp);
    }

    public void four(View v) {
        if (display.getText().equals("0"))
            temp = "4";
        else
            temp = display.getText() + "4";
        display.setText(temp);
    }

    public void five(View v) {
        if (display.getText().equals("0"))
            temp = "5";
        else
            temp = display.getText() + "5";
        display.setText(temp);
    }

    public void six(View v) {
        if (display.getText().equals("0"))
            temp = "6";
        else
            temp = display.getText() + "6";
        display.setText(temp);
    }

    public void seven(View v) {
        if (display.getText().equals("0"))
            temp = "7";
        else
            temp = display.getText() + "7";
        display.setText(temp);
    }

    public void eight(View v) {
        if (display.getText().equals("0"))
            temp = "8";
        else
            temp = display.getText() + "8";
        display.setText(temp);
    }

    public void nine(View v) {
        if (display.getText().equals("0"))
            temp = "9";
        else
            temp = display.getText() + "9";
        display.setText(temp);
    }

    public void add(View v) {
        if (display.getText().equals("0"))
            temp = "+";
        else
            temp = display.getText() + "+";
        display.setText(temp);
    }

    public void sub(View v) {
        if (display.getText().equals("0"))
            temp = "-";
        else
            temp = display.getText() + "-";
        display.setText(temp);
    }

    public void mul(View v) {
        if (display.getText().equals("0"))
            temp = "*";
        else
            temp = display.getText() + "*";
        display.setText(temp);
    }

    public void div(View v) {
        if (display.getText().equals("0"))
            temp = "/";
        else
            temp = display.getText() + "/";
        display.setText(temp);
    }

    public void calculate(View v) throws JSONException, IOException {
        String toWrite = (String) display.getText();

        Subs subs = new Subs();
        Tokenizer tok = new SimpleTokenizer("("+ toWrite+")");
        Log.d("Broken", "rest in peace" + toWrite);
        Expression exp = parseExp(tok);
/*
        JSONObject obj = new JSONObject();

        List<String> list = new ArrayList<>();
        list.add(toWrite);

        obj.put("se", "test");
        FileOutputStream file = openFileOutput("test.json", MODE_APPEND);

        Log.d("es", "C:\\\\test.json");

        file.write(toWrite);
        file.close();
*/

        try {
            OutputStreamWriter write = new OutputStreamWriter(openFileOutput("test.txt", MODE_APPEND));
            write.append(toWrite);
            write.close();
        } catch (IOException e) {
            Log.e("Broken", "rest in peace" + e.toString());
        }

        display.setText("" + exp.evaluate(subs));
    }

    public void replay(View v) throws IOException {
        String ret = "";
        try {
            InputStream read = openFileInput("test.txt");

            if (read != null) {
                InputStreamReader reader = new InputStreamReader(read);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {

        }

        display.setText(ret);

        Log.d("123456", ret);
    }
}
