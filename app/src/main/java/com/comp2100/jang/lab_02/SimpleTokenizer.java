package com.comp2100.jang.lab_02;


public class SimpleTokenizer extends Tokenizer {

    String text; // the text to be tokenizer
    int pos; // position the tokenizer is currently pointing at.
    Object current; // the current token that has just been tokenized, noting
    // the "pos" will point just after the text of this token

    public SimpleTokenizer(String text) {
        this.text = text;
        pos = 0;
        next();
    }

    @Override
    boolean hasNext() {
        return current != null;
    }

    @Override
    Object current() {
        return current;
    }

    @Override
    void next() {
        char c = 0;
        int textlen = text.length();


        while(pos < textlen)
        {
            c = text.charAt(pos);
            if(c == ' ') pos++;
            else break;
        }
        if (pos < textlen)
            c = text.charAt(pos);


        // consume the white space



        // modify this code


        if (pos >= textlen) {
            current = null;
            return;
        }
        switch (c){
            case '(': pos++; current = "("; break;
            case ')': pos++; current = ")"; break;
            case '*': pos++; current = "*"; break;
            case '+': pos++; current = "+"; break;
            case '-': pos++; current = "-"; break;
            case '/': pos++; current = "/"; break;
            case ' ': pos++; current = ""; break;
            case '\n': pos++; current = "\n"; break;

            default:
                if (isNumber(c)) {
                    String res = "" + c;
                    pos++;
                    c = text.charAt(pos);
                    while (pos < textlen && isNumber(c)) {
                        res += c;
                        pos++;
                        c = text.charAt(pos);
                    }
                    current = Integer.parseInt(res);
                    return;
                }

                throw new Error("Tried to parse unrecognised character");
        }

    }

    boolean isNumber(char c) {
        if ((int) c >= 48 && (int) c <= 57)
            return true;
        else
            return false;
    }

}
