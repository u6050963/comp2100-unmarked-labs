package com.comp2100.jang.lab_02;

/**
 * Created by Damanvir Singh on 02-May-17.
 */
public abstract class Expression {

    abstract public String show();
    abstract public int evaluate(Subs subs);
    abstract public int size();
    abstract public int height();
    abstract public int operators();
    abstract public Expression simplify();

    public static AddExp add(Expression l, Expression r) {
        return new AddExp(l, r);
    }

    public static DivExp div(Expression l, Expression r) {
        return new DivExp(l, r);
    }

    public static LitExp lit(int v) {
        return new LitExp(v);
    }

    public static MultExp mul(Expression l, Expression r) {
        return new MultExp(l, r);
    }

    public static SubExp sub(Expression l, Expression r) {
        return new SubExp(l, r);
    }

    public static VarExp var(String v) {
        return new VarExp(v);
    }


    public static Expression parseExp (Tokenizer tok) {
        Expression term = parseTerm(tok);
        if (tok.current() == "+") {
            tok.next();
            Expression exp = parseExp(tok);
            return new AddExp(term, exp);
        } else if (tok.current() == "-") {
            tok.next();
            Expression exp = parseExp(tok);
            return new SubExp(term, exp);
        } else {
            return term;
        }
    }

    public static Expression parseTerm(Tokenizer tok) {
        Expression fact = parseFactor(tok);
        if (tok.current() == "*") {
            tok.next();
            Expression term = parseTerm(tok);
            return new MultExp(fact, term);
        } else if (tok.current() == "/") {
            tok.next();
            Expression term = parseTerm(tok);
            return new DivExp(fact, term);
        } else {
            return fact;
        }
    }

    public static Expression parseFactor(Tokenizer tok) {
        if (tok.current() instanceof Integer) {
            Expression exp = new LitExp((Integer) tok.current());
            tok.next();
            return exp;
        } else if (tok.current() == "(") {
            tok.next();
            Expression exp = parseExp(tok);
            tok.parse(")");
            return exp;
        } else {
            Expression exp = new VarExp((String) tok.current());
            return exp;
        }
    }

}
