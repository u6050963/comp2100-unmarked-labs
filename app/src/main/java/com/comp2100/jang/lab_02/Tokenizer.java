package com.comp2100.jang.lab_02;


public abstract class Tokenizer {

    abstract boolean hasNext();

    abstract Object current();

    abstract void next();

    public void parse(Object o)  {
        if (current() == null || !current().equals(o))
            throw new Error("Tried to parse Token "+o+" when it was "+current()); // normally one would use exceptions in such cases, however,
        // it just makes code simpler as you don't need to deal with the exception
        // in calling code
        next();
    }
}
