package com.comp2100.jang.lab_02;

/**
 * Created by Damanvir Singh on 02-May-17.
 */
public class MultExp extends Operators {

    Expression left, right;

    public MultExp(Expression l, Expression r) {
        left = l;
        right = r;
    }

    public String show() {

        return "(" + left.show() + " * " + right.show() + ")";
    }

    public int evaluate(Subs subs) {
        return left.evaluate(subs) * right.evaluate(subs);
    }

    public int size() {
        return left.size() + right.size() + 1;
    }

    public int height() {
        if (left.height() > right.height()) {
            return left.height() + 1;
        } else {
            return left.height() + 1;
        }
    }

    public int operators() {
        return left.operators() + right.operators() + 1;
    }

    public Expression simplify() {
        left = left.simplify();
        right = right.simplify();
        if (left instanceof LitExp && right instanceof LitExp) {
            int x = ((LitExp)left).value;
            int y = ((LitExp)right).value;
            return lit(x*y);
        }
        return this;
    }

}
