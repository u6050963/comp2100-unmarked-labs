package com.comp2100.jang.lab_02;

/**
 * Created by Damanvir Singh on 02-May-17.
 */

public class VarExp extends Variables {

    String name;

    public VarExp(String n) {
        name = n;
    }

    public String show() {

        return "" + name;
    }

    public int evaluate(Subs subs) {
        return subs.get(name);
    }

    public Expression simplify() {
        return this;
    }

}
