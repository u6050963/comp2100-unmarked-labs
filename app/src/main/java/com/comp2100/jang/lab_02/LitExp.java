package com.comp2100.jang.lab_02;

/**
 * Created by Damanvir Singh on 02-May-17.
 */
public class LitExp extends Variables {

    int value;

    public LitExp (int v) {
        value = v;
    }

    public String show() {

        return "" + value;
    }


    public int evaluate(Subs subs) {
        return value;
    }

    public Expression simplify() {
        return this;
    }

}
