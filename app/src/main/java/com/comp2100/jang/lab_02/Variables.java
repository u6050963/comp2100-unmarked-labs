package com.comp2100.jang.lab_02;

/**
 * Created by Damanvir Singh on 02-May-17.
 */
public abstract class Variables extends Expression {

    public int size() {
        return 1;
    }

    public int height() {
        return 1;
    }

    public int operators() {
        return 0;
    }

}
